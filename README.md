# B2 Projet 2022

## Mise en place technique

### Overview

Vous devez héberger un service accessible publiquement sur Internet.

Plusieurs éléments sont attendus :

- 1 service hébergé
- utilisation de 3 machines ou plus
- setup de 3 technos/techniques non vues en cours
- ajout de 3 éléments de sécu
- un moyen pour déployer automatiquement la solution

> Vous pouvez vous écarter légèrement de cette liste si ce que vous produisez à côté est digne d'un deuxième année qui s'est investi.

### Choix solution

Le choix du service à héberger n'est placé sous aucune contrainte : quelque chose que vous avez vous-mêmes développé, une solution libre et open source trouvée en ligne, etc.

## Notation

Deux rendus sont attendus et seront notés : oral et écrit.

### Oral

**L'oral, jusqu'à nouvel ordre, se déroulera le 07 Avril, sur la dernière séance de soutien.**

Pour l'oral, vous serez notés sur 4 critères, chacun noté sur 5 :

- pertinence problématique
  - si votre sujet est digne de 2003, exit
- qualité de la prés
  - si on se fait chier en vous écoutant, exit
- démo technique
  - si vous ne savez que blabla, sans réalisation technique, exit
- challenge
  - si ça n'a pas demandé de travail pour vous, ou très peu, en tant que B2 en fin d'année, exit

### Ecrit

**L'écrit, jusqu'à nouvel ordre, est à rendre le 06 Avril à 23h59.**

Un dépôt git est attendu. Il indique :

- comment joindre votre service qui doit être hébergé publiquement
- comment déployer nous-mêmes le service
  - d'éventuels prérequis
  - instructions pour déclencher le déploiement

Le rendu demandé est si court et si simple, aucun retard ne sera toléré. Genre aucun.


